#ifndef SDIPC_DEVICE_H
#define SDIPC_DEVICE_H
#include <glib.h>

typedef struct Protocol {
    gchar *name;
    gchar *transport;
    guint16 port;
} Protocol;

Protocol *ProtocolNew(gchar *name, gchar *transport, guint16 port);
void ProtocolFree(Protocol *self);
void ProtocolVariantAdd(Protocol *self, GVariantBuilder *bld);

typedef struct Device {
    gchar *uuid;
    gchar *name;
    gchar *type;
    gchar *ip;
    GList *protocols;
} Device;

Device *DeviceNew();
void DeviceFree(Device *self);
void DeviceVariantAdd(Device *self, GVariantBuilder *bld);

#endif // SDIPC_DEVICE_H
