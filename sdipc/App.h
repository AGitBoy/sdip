#ifndef SDIPC_APP_H
#define SDIPC_APP_H
#include <glib.h>
#include <gio/gio.h>
#include "sdipc.h"

typedef struct App App;
struct App {
    GHashTable *tab;
    GMainLoop *loop;
    GSocket *sendsock, *listensock;
    GSocketAddress *sendaddr, *listenaddr;
    GIOChannel *socklisten;
    Sdipc *skel;
    guint dbus_id, listensock_id;
};

App *AppNew();
void AppRun(App *self);
void AppFree(App *self);
void AppBusAcquired(GDBusConnection *connection, const gchar *name, gpointer user_data);
void AppNameAcquired(GDBusConnection *connection, const gchar *name, gpointer user_data);
void AppNameLost(GDBusConnection *connection, const gchar *name, gpointer user_data);
gboolean AppGetDevices(Sdipc *skel, GDBusMethodInvocation *inv, gpointer user_data);
gboolean AppHandleListener(GIOChannel *channel, GIOCondition condition, gpointer user_data);
gboolean AppSendGet(gpointer user_data);

#endif
