#include "App.h"
#include "Device.h"
#include "cfg.h"
#include <stdio.h>

// private function definitions
void AppHandleBuf(App *self, gchar *buf, gchar *ip);

// useful for callback functions
#define SELF_FROM_USERDATA App *self = (App *)user_data;

/*
 * Application constructor
 *
 * Initializes variables and sets up the callbacks for
 * data handling
 */
App *AppNew()
{
    App *self = malloc(sizeof(App));
    g_assert(self != NULL);
    self->loop = g_main_loop_new(NULL, FALSE);

    GError *err = NULL;

    self->tab = g_hash_table_new_full(&g_str_hash, &g_str_equal, NULL,
                                      (GDestroyNotify)DeviceFree);

    self->dbus_id = g_bus_own_name(G_BUS_TYPE_SYSTEM, "org.sdip.Sdipc",
                                   G_BUS_NAME_OWNER_FLAGS_NONE, AppBusAcquired,
                                   AppNameAcquired, AppNameLost, self, NULL);

    GInetAddress *tmpaddr = g_inet_address_new_from_string(MULTI_ADDRESS);

    self->listenaddr = G_SOCKET_ADDRESS(g_inet_socket_address_new(
                g_inet_address_new_any(G_SOCKET_FAMILY_IPV4), LISTEN_PORT));

    self->listensock = g_socket_new(G_SOCKET_FAMILY_IPV4,
                                    G_SOCKET_TYPE_DATAGRAM,
                                    G_SOCKET_PROTOCOL_UDP,
                                    &err);
    g_assert(err == NULL);

    g_socket_join_multicast_group(self->listensock, tmpaddr, FALSE, NULL, &err);
    g_assert(err == NULL);

    g_socket_bind(self->listensock, self->listenaddr, TRUE, &err);
    g_assert(err == NULL);

    self->sendaddr = G_SOCKET_ADDRESS(g_inet_socket_address_new(tmpaddr, PORT));

    self->sendsock = g_socket_new(G_SOCKET_FAMILY_IPV4, G_SOCKET_TYPE_DATAGRAM,
                                  G_SOCKET_PROTOCOL_UDP, &err);
    g_assert(err == NULL);

    // add watch to the listener socket
    self->socklisten = g_io_channel_unix_new(g_socket_get_fd(self->listensock));
    self->listensock_id = g_io_add_watch(self->socklisten, G_IO_IN, AppHandleListener, self);

    AppSendGet(self);

    // periodically send a GET request
    g_timeout_add_seconds(GET_IVAL, AppSendGet, self);

    return self;
}

void AppFree(App *self)
{
    g_hash_table_unref(self->tab);
    g_bus_unown_name(self->dbus_id);
    g_source_remove(self->listensock_id);
    g_io_channel_unref(self->socklisten);
    g_main_loop_quit(self->loop);
    g_main_loop_unref(self->loop);
    free(self);
}

void AppRun(App *self)
{
    g_main_loop_run(self->loop);
}

void AppBusAcquired(GDBusConnection *connection, const gchar *name,
                    gpointer user_data)
{
    SELF_FROM_USERDATA;
}

void AppNameAcquired(GDBusConnection *connection, const gchar *name,
                     gpointer user_data)
{
    SELF_FROM_USERDATA;
    GError *error = NULL;

    self->skel = sdipc_skeleton_new();

    g_signal_connect(self->skel, "handle-get-devices",
                     G_CALLBACK(AppGetDevices), self);

    g_dbus_interface_skeleton_export(G_DBUS_INTERFACE_SKELETON(self->skel),
                                     connection, "/org/sdip/Sdipc", &error);

    if (error != NULL) {
        g_printerr("failed to export object: %s\n", error->message);
        g_error_free(error);
    }
}

void AppNameLost(GDBusConnection *connection, const gchar *name,
                 gpointer user_data)
{
    SELF_FROM_USERDATA;
}

// dbus IPC callback
gboolean AppGetDevices(Sdipc *skel, GDBusMethodInvocation *inv, gpointer user_data)
{
    SELF_FROM_USERDATA;
    GHashTableIter iter;
    GVariantBuilder bld;
    GVariant *ret;
    Device *dev;
    gchar *uuid;

    // convert hash table into GVariant
    g_variant_builder_init(&bld, G_VARIANT_TYPE("a{sa{sv}}"));
    g_hash_table_iter_init(&iter, self->tab);

    while (g_hash_table_iter_next(&iter, (gpointer *)&uuid, (gpointer *)&dev)) {
        g_variant_builder_open(&bld, G_VARIANT_TYPE("{sa{sv}}"));
        g_variant_builder_add(&bld, "s", uuid);
        DeviceVariantAdd(dev, &bld);
        g_variant_builder_close(&bld);
    }

    ret = g_variant_builder_end(&bld);
    g_variant_print(ret, FALSE);
    sdipc_complete_get_devices(skel, inv, ret);

    return TRUE;
}

// handle message listener
gboolean AppHandleListener(GIOChannel *channel, GIOCondition condition,
                           gpointer user_data)
{
    SELF_FROM_USERDATA;
    GError *err = NULL;
    GSocketAddress *addr;
    gchar buf[MAX_REQUEST_SIZE];
    gchar *ip;
    gsize bytes;

    // prevent leftover data from being in buf
    memset(buf, 0, MAX_REQUEST_SIZE);

    // check for channel close
    if (condition & G_IO_HUP)
        return FALSE;

    // get the data from the socket
    g_socket_receive_from(self->listensock, &addr, buf, sizeof(buf), NULL, &err);

    // error handling
    if (err != NULL) {
        g_printerr("failed to recieve data: %s\n", err->message);
        g_error_free(err);
        return TRUE;
    }

    // get ip address of the server
    ip = strdup(g_inet_address_to_string(g_inet_socket_address_get_address(
                                      G_INET_SOCKET_ADDRESS(addr))));

    // parse request
    AppHandleBuf(self, buf, ip);

    return TRUE;
}

// sends a GET message
gboolean AppSendGet(gpointer user_data)
{
    SELF_FROM_USERDATA;
    GError *err = NULL;

    g_socket_send_to(self->sendsock, self->sendaddr, REQUEST_GET,
                     strlen(REQUEST_GET), FALSE, &err);

    if (err != NULL) {
        fprintf(stderr, "sdipc: %s\n", err->message);
        return TRUE;
    }

    return TRUE;
}

// macros for AppHandleBuf
#define MALFORMED_REQUEST                                   \
    {                                                       \
        fprintf(stderr, "sdipc: malformed request\n");      \
        return;                                             \
    }

#define HEADER_ASSIGN(var)                          \
    tabtok = strtok_r(NULL, "\t", &tabendtok);      \
    if (tabtok == NULL)                             \
        MALFORMED_REQUEST;                          \
    var = tabtok;

#define NEXTLINE lntok = strtok_r(NULL, "\n", &lnendtok)

/*
 * Parse a response and modify the table accordingly
 */
void AppHandleBuf(App *self, gchar *buf, gchar *ip)
{
    gchar *lntok, *tabtok, *lnendtok, *tabendtok, *pname, *ptransport;
    gint pport;

    Device *dev = DeviceNew();

    lntok = strtok_r(buf, "\n", &lnendtok);

    if (lntok == NULL)
        MALFORMED_REQUEST;

    if (strncmp(lntok, RET_HEADER, strlen(RET_HEADER)) == 0 ||
        strncmp(lntok, NEW_HEADER, strlen(NEW_HEADER)) == 0) {
        dev->ip = ip;

        for (NEXTLINE; lntok != NULL; NEXTLINE) {
            tabtok = strtok_r(lntok, "\t", &tabendtok);

            if (tabtok == NULL)
                MALFORMED_REQUEST;

            if (strncmp(tabtok, "name", 4) == 0) {
                HEADER_ASSIGN(dev->name);
            } else if (strncmp(tabtok, "uuid", 4) == 0) {
                HEADER_ASSIGN(dev->uuid);
            } else if (strncmp(tabtok, "type", 4) == 0) {
                HEADER_ASSIGN(dev->type);
            } else {
                // add a protocol
                pname = tabtok;
                ptransport = strtok_r(NULL, "\t", &tabendtok);
                if (ptransport == NULL)
                    MALFORMED_REQUEST;

                tabtok = strtok_r(NULL, "\t", &tabendtok);

                if (tabtok == NULL)
                    MALFORMED_REQUEST;

                pport = atoi(tabtok);

                dev->protocols = g_list_append(dev->protocols, ProtocolNew(
                                                   pname, ptransport, pport));
            }
        }

        g_hash_table_insert(self->tab, dev->uuid, dev);
    } else if (strncmp(lntok, DEL_HEADER, strlen(DEL_HEADER)) == 0) {
        lntok = strtok_r(NULL, "\n", &lnendtok);
        if (lntok == NULL)
            MALFORMED_REQUEST;

        g_hash_table_remove(self->tab, lntok);
    } else
        MALFORMED_REQUEST;
}
