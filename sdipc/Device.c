#include "Device.h"
#include <stdio.h>

// Initializer for Protocol struct
Protocol *ProtocolNew(gchar *name, gchar *transport, guint16 port)
{
    Protocol *self = malloc(sizeof(Protocol));
    g_assert(self != NULL);

    self->name = name;
    self->transport = transport;
    self->port = port;

    return self;
}

// Destructor for Protocol struct
void ProtocolFree(Protocol *self)
{
    free(self);
}

/*
 * adds Protocol object to container
 *
 * references:
 * https://developer.gnome.org/glib/stable/gvariant-format-strings.html
 * https://developer.gnome.org/glib/stable/glib-GVariant.html
 */
void ProtocolVariantAdd(Protocol *self, GVariantBuilder *bld)
{
    g_variant_builder_open(bld, G_VARIANT_TYPE("a{sv}"));

    g_variant_builder_add(bld, "{sv}", "name", g_variant_new_string(self->name));
    g_variant_builder_add(bld, "{sv}", "transport", g_variant_new_string(self->transport));
    g_variant_builder_add(bld, "{sv}", "port", g_variant_new_uint16(self->port));

    g_variant_builder_close(bld);
}

Device *DeviceNew()
{
    Device *self = malloc(sizeof(Device));
    g_assert(self != NULL);

    self->protocols = NULL;

    return self;
}

void DeviceFree(Device *self)
{
    g_list_free_full(self->protocols, (GDestroyNotify)ProtocolFree);
    free(self);
}

/*
 * add Device object to container
 *
 * references:
 * https://developer.gnome.org/glib/stable/gvariant-format-strings.html
 * https://developer.gnome.org/glib/stable/glib-GVariant.html
 */
void DeviceVariantAdd(Device *self, GVariantBuilder *bld)
{
    g_variant_builder_open(bld, G_VARIANT_TYPE("a{sv}"));

    g_variant_builder_add(bld, "{sv}", "uuid", g_variant_new_string(self->uuid));
    g_variant_builder_add(bld, "{sv}", "name", g_variant_new_string(self->name));
    g_variant_builder_add(bld, "{sv}", "type", g_variant_new_string(self->type));
    g_variant_builder_add(bld, "{sv}", "ip", g_variant_new_string(self->ip));

    g_variant_builder_open(bld, G_VARIANT_TYPE("{sv}"));

    g_variant_builder_add(bld, "s", "protocols");

    GVariantBuilder subb;
    GVariant *protov;
    g_variant_builder_init(&subb, G_VARIANT_TYPE("aa{sv}"));

    g_list_foreach(self->protocols, (GFunc)ProtocolVariantAdd, &subb);

    protov = g_variant_builder_end(&subb);

    g_variant_builder_add(bld, "v", protov);

    g_variant_builder_close(bld);
    g_variant_builder_close(bld);
}
