#ifndef SDIPC_CONFIG_H
#define SDIPC_CONFIG_H

// multicast address to send to
#define MULTI_ADDRESS "239.255.255.245"

// port number
#define PORT 71

// port number for event listener
#define LISTEN_PORT 72

// maximum request size
#define MAX_REQUEST_SIZE 65467

// minimum request size
#define MIN_REQUEST_SIZE 9

// GET request string
#define REQUEST_GET "SDIP\tGET\n"

// Request headers
#define RET_HEADER "SDIP\tRET"
#define NEW_HEADER "SDIP\tNEW"
#define DEL_HEADER "SDIP\tDEL"

// GET interval
#define GET_IVAL 60

#endif // SDIPC_CONFIG_H
