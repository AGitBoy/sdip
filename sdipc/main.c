#include "cfg.h"
#include "App.h"

int main()
{
    App *app = AppNew();
    AppRun(app);
    AppFree(app);
    return 0;
}
