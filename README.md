# Simple Device Identification Protocol (SDIP)
SDIP is a networking protocol operating at the application layer of the
TCP/IP model, designed, simply put, to discover hosts providing network services
on the local network. This plays a similar role to the existing SSDP protocol,
which does nearly the exact same thing, albeit in a differing manner.
