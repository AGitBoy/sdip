#include "App.h"
#include "util.h"

#include <unistd.h>

#include <sys/queue.h>

#include <errno.h>
#include <err.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <uuid.h>

/*
 * Constructor for App class
 *
 * Allocates and returns an instance of App on the heap.
 * It also initializes the configuration for the program,
 * and opens sockets, binds to addresses, and sets socket
 * options needed for the program to correctly function.
 */
App *AppNew(char *file) {
    App *self = xmalloc(sizeof(App));
    self->cfg = ConfigNew(file);

    // generate the UUID string
    uuid_t u;
    uuid_generate(u);
    uuid_unparse_lower(u, self->uuid);

    if (AppResponseLength(self) > MAX_REQUEST_SIZE)
        errx(1, "response size is over the limit of %i bytes", MAX_REQUEST_SIZE);

    // open a UDP socket
    if ((self->sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        err(1, "socket");

    self->serv.sin_family = AF_INET;
    self->serv.sin_addr.s_addr = htonl(INADDR_ANY);
    self->serv.sin_port = htons(PORT);

    u_int on = 1;

    // allows program to be restarted
    if (setsockopt(self->sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&on,
                   sizeof(on)) < 0)
        err(1, "setsockopt: SO_REUSEADDR");

    // bind to ip, requires root privilages on most OS setups
    if (bind(self->sockfd, (const struct sockaddr *)&self->serv,
             sizeof(self->serv)) < 0)
        err(1, "bind");

    // subscribe to a multicast group
    self->mreq.imr_multiaddr.s_addr = inet_addr(ADDRESS);
    self->mreq.imr_interface.s_addr = htonl(INADDR_ANY);

    if (setsockopt(self->sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                   (char *)&self->mreq, sizeof(self->mreq)) < 0)
        err(1, "setsockopt: IP_ADD_MEMBERSHIP");

    if (setsockopt(self->sockfd, IPPROTO_IP, IP_MULTICAST_LOOP, (char *)&on,
                   sizeof(on)) < 0)
        err(1, "setsockopt: IP_MULTICAST_LOOP");

    // open client socket
    if ((self->clisock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        err(1, "socket");

    self->client.sin_family = AF_INET;
    self->client.sin_addr.s_addr = inet_addr(ADDRESS);
    self->client.sin_port = htons(PORT_CLIENT);

    return self;
}

/*
 * Destructor for App class
 *
 * Should also close any open file descriptors, and handle children,
 * in general, it acts like a destructor for the program.
 */
void AppFree(App *self)
{
    close(self->sockfd);
    ConfigFree(self->cfg);
    free(self);
}

/*
 * The main loop of the program.
 *
 * Simply recieves UDP datagrams, checks if they are valid SDIP requests,
 * and responds accordingly if they are, sending a datagram with the response.
 *
 * This could do a better job with data security in preventing buffer overflows.
 */
void AppNetLoop(App *self)
{
    AppSendClient(self, NEW);
    char *r;

    for (;;) {
        self->len = sizeof(self->src);

        // get request
        self->bytes = recvfrom(self->sockfd, &self->buffer, MAX_REQUEST_SIZE, 0,
                               &self->src, &self->len);

        // checks if the request is a valid GET request
        if (strncmp(self->buffer, REQUEST_GET, strlen(REQUEST_GET)) == 0) {
            AppSendClient(self, RET);
        } else if (! (strncmp(self->buffer, "SDIP", strlen("SDIP")) == 0)) {
            // log a malformed request
            struct sockaddr_in *sin = (struct sockaddr_in *)&self->src;
            char str[INET6_ADDRSTRLEN];

            // convert binary address to a human readable address
            if (inet_ntop(self->src.sa_family, &sin->sin_addr,
                          str, INET6_ADDRSTRLEN) == NULL) {
                warn("inet_ntop");
                warnx("malformed request");
            } else {
                warnx("malformed request from %s", str);
            }
        }
    }
}

// send out a 'NEW', 'DEL', or 'RET' request
void AppSendClient(App *self, Request req)
{
    char *str = AppGenerateResponse(self, req);

    if (sendto(self->clisock, str, strlen(str), 0,
               (struct sockaddr *)&self->client, sizeof(self->client)) < 0)
        err(1, "sendto");

    free(str);
}

/* Generates a string to be sent as a datagram. */
char *AppGenerateResponse(App *self, Request req)
{
    char *resp;
    if (req == RET || req == NEW) {
        resp = xmalloc(AppResponseLength(self) * sizeof(char));

        char *reqstr;
        if (req == RET)
            reqstr = "RET";
        else
            reqstr = "NEW";

        sprintf(resp, "SDIP\t%s\nname\t%s\ntype\t%s\nuuid\t%s\n", reqstr,
                self->cfg->name, self->cfg->type, self->uuid);

        Protocol *proto;
        TAILQ_FOREACH(proto, &self->cfg->protocols, ent) {
            char *x = malloc(strlen(proto->name) + numdigits(proto->port)
                            + strlen(proto->transport) + 4);

            sprintf(x, "%s\t%s\t%i\n", proto->name, proto->transport, proto->port);
            strcat(resp, x);
            free(x);
        }
    } else if (req == DEL) {
        resp = xmalloc(11 + strlen(self->uuid));

        sprintf(resp, "SDIP\tDEL\n%s\n", self->uuid);
    } else
        errx(1, "AppGenerateResponse: Invalid request type");

    return resp;
}

/*
 * Calculate length of response based on the known format that the reponse has,
 * used for allocating a string for the response.
 */
int AppResponseLength(App *self)
{
    // size of the header segment
    int i = 27 + strlen(self->cfg->name) + strlen(self->cfg->type) + strlen(self->uuid);

    Protocol *proto;
    TAILQ_FOREACH(proto, &self->cfg->protocols, ent) {
        i += strlen(proto->name) + numdigits(proto->port)
            + strlen(proto->transport) + 3;
    }

    return ++i;
}
