#ifndef SDIPD_APP_H
#define SDIPD_APP_H
#include <netinet/in.h>
#include <uuid.h>

#include "cfg.h"
#include "Config.h"

typedef enum Request Request;
enum Request {
    RET,
    NEW,
    DEL
};

// structure implementing the application instance
typedef struct App App;
struct App {
    Config *cfg;
    char uuid[37];

    ssize_t bytes;
    char buffer[MAX_REQUEST_SIZE + 20];  // add 20 bytes of buffer space

    int sockfd, clisock;
    socklen_t len;
    struct sockaddr_in serv, client;
    struct sockaddr src;
    struct ip_mreq mreq;
};

// application initializer
App *AppNew(char *file);

// free application instance
void AppFree(App *self);

// main network loop
void AppNetLoop(App *self);

// send 'NEW' or 'DEL' request
void AppSendClient(App *self, Request req);

// generates resposne
char *AppGenerateResponse(App *self, Request req);

// calculate response length
int AppResponseLength(App *self);

#endif // SDIPD_APP_H
