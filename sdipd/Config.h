#ifndef SDIP_CONF_H
#define SDIP_CONF_H
#include <sys/queue.h>
#include <stddef.h>
#include <stdio.h>
#include <ucl.h>

typedef struct Protocol Protocol;
struct Protocol {
    char *name;
    char *transport;
    int port;
    TAILQ_ENTRY(Protocol) ent;
};

// initializer for Protocol class
Protocol *ProtocolNew(const ucl_object_t *obj);

typedef struct Config Config;
struct Config {
    char *name;
    char *type;
    TAILQ_HEAD(protocolList, Protocol) protocols;
};

// load application configuration from file
Config *ConfigNew(char *file);
void ConfigFree(Config *self);

#endif
