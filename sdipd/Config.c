#include "cfg.h"
#include "Config.h"
#include "util.h"

#include <sys/queue.h>
#include <err.h>

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include <ucl.h>

// typecheck for ucl object type or warns and forces conversion
char *ucl_stringchk(const ucl_object_t *obj, const char *key, char *file)
{
    if (ucl_object_type(obj) == UCL_STRING) {
        return strdup(ucl_object_tostring(obj));
    } else {
        warnx("%s: key '%s' has incorrect type: expected %s, got %s",
              file, key, ucl_object_type_to_string(UCL_STRING),
              ucl_object_type_to_string(ucl_object_type(obj)));

        return strdup(ucl_object_tostring_forced(obj));
    }
}

// create Protocol object from ucl object
Protocol *ProtocolNew(const ucl_object_t *obj)
{
    const ucl_object_t *cur;
    const char *tmp;
    Protocol *self = xmalloc(sizeof(Protocol));

    self->name = strdup(ucl_object_key(obj));

    cur = ucl_obj_get_key(obj, "transport");
    if (cur != NULL) {
        if (ucl_object_tostring_safe(cur, &tmp))
            self->transport = strdup(tmp);
        else
            err(1, "invalid type for key 'transport' in object '%s'\n", self->name);
    } else {
        err(1, "no key 'transport' in object '%s'", self->transport);
    }

    cur = ucl_obj_get_key(obj, "port");
    if (cur != NULL) {
        int64_t i;
        if (ucl_object_toint_safe(cur, &i))
            self->port = i;
        else
            err(1, "invalid type for key 'port' in object '%s'\n", self->name);
    } else {
        err(1, "no key 'port' in object '%s'\n", self->name);
    }

    return self;
}

// free Protocol object
void ProtocolFree(Protocol *self)
{
    free(self->name);
    free(self->transport);
    free(self);
}

// initalize configuration
Config *ConfigNew(char *file)
{
    Config *self = xmalloc(sizeof(Config));
    self->name = "default";
    self->type = "generic";
    TAILQ_INIT(&self->protocols);

    struct ucl_parser *cfg;
    const ucl_object_t *cur;
    ucl_object_t *obj;

    cfg = ucl_parser_new(UCL_PARSER_KEY_LOWERCASE);

    // load config file
    if (!ucl_parser_add_file(cfg, file))
        err(1, "%s: %s", file, strerror(errno));

    obj = ucl_parser_get_object(cfg);

    if (obj == NULL)
        err(1, "%s: %s", file, ucl_parser_get_error(cfg));

    // 'name' key
    cur = ucl_obj_get_key(obj, "name");
    if (cur != NULL)
        self->name = ucl_stringchk(cur, "name", file);
    else
        warnx("%s: required key 'name' has no value assigned: using default value", file);

    // 'type' key
    cur = ucl_obj_get_key(obj, "type");
    if (cur != NULL)
        self->type = ucl_stringchk(cur, "type", file);
    else
        warnx("%s: required key 'type' has no value assigned: using default value", file);

    // 'protocols' key
    cur = ucl_obj_get_key(obj, "protocols");
    if (cur != NULL && ucl_object_type(cur) == UCL_OBJECT) {
        const ucl_object_t *iobj;
        ucl_object_iter_t it = NULL;
        Protocol *proto;

        while ((iobj = ucl_iterate_object(cur, &it, true))) {
            proto = ProtocolNew(iobj);
            TAILQ_INSERT_TAIL(&self->protocols, proto, ent);
        }
    } else {
        warnx("%s: required key 'protocols' has no value assigned or is of an incorrect type: using empty", file);
    }

    // free memory
    ucl_parser_free(cfg);
    ucl_object_unref(obj);

    return self;
}

// free Config object
void ConfigFree(Config *self)
{
    Protocol *i;
    while ((i = TAILQ_FIRST(&self->protocols))) {
        TAILQ_REMOVE(&self->protocols, i, ent);
        ProtocolFree(i);
    }

    free(self->name);
    free(self->type);
    free(self);
}
