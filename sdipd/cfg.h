#ifndef SDIP_CONFIG_H
#define SDIP_CONFIG_H

// debug mode for program
#define DEBUG

// pid file
#define PIDFILE "/var/run/sdipd.pid"

// config file
#define CONF "/etc/sdipd.conf"

// multicast address to bind to
#define ADDRESS "239.255.255.245"

// port number
#define PORT 71

// port number for client
#define PORT_CLIENT 72

/*
 * Max size of a single request is 65467 bytes
 *
 * This is the maximum size of an IPv4 packet (65535 bytes) minus
 * the sum of the maximum IPv4 header size (60 bytes) and the size
 * of a UDP header (8 bytes)
 */
#define MAX_REQUEST_SIZE 65467

// minimum request size
#define MIN_REQUEST_SIZE 9

// GET request string
#define REQUEST_GET "SDIP\tGET\n"

#endif // SDIP_CONFIG_H
