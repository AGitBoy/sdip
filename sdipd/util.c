// various utility functions
#include "util.h"
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <string.h>
#include <err.h>

void *xmalloc(size_t size)
{
    void *ptr = malloc(size);
    if (ptr == NULL)
        err(1, "xmalloc");

    return ptr;
}

int numdigits(int i)
{
    int digits;
    for (digits = 0; i >= 1;) {
        i /= 10;
        digits++;
    }

    return digits;
}
