#include "cfg.h"
#include "App.h"

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <err.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// Application instance
App *app;

void usage()
{
    fprintf(stderr, "usage: sdipd [-d] [-f FILE]\n");
    exit(1);
}

void exithdl()
{
    AppSendClient(app, DEL);
    AppFree(app);
}

void sigwrap(int i)
{
    exit(1);
}

int main(int argc, char **argv)
{
    int dflag = 0;
    char *cfg = CONF;

    int opt;
    char *arg;

    while ((opt = getopt(argc, argv, ":df:")) != -1) {
        switch(opt) {
            case 'd':  // disable daemonization
                dflag = 1;
                break;
            case 'f':  // specify config file
                cfg = optarg;
                break;
            case '?':
                usage();
        }
    }

    if ((!dflag) && (daemon(0, 1) < 0))
        err(1, "daemon");

    app = AppNew(cfg);

    // setup signal handlers
    atexit(&exithdl);
    signal(SIGINT, &sigwrap);
    signal(SIGABRT, &sigwrap);
    signal(SIGTERM, &sigwrap);

    AppNetLoop(app);

    return 0;
}
