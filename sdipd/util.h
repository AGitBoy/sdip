#ifndef __UTIL_H_
#define __UTIL_H_
#include <stddef.h>
#include <stdio.h>

// malloc or die
void *xmalloc(size_t size);

// count number of digits in an integer
int numdigits(int i);

#endif // __UTIL_H_
