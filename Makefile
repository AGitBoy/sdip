# root Makefile for sdip
MAKE = make

SDIPD_DIR = sdipd
SDIPD_BIN = $(SDIPD_DIR)/sdipd

SDIPC_DIR = sdipc
SDIPC_BIN = $(SDIPC_DIR)/sdipc

all: $(SDIPD_BIN) $(SDIPC_BIN)

.PHONY: $(SDIPD_BIN) $(SDIPC_BIN) $(SDIPD_BIN)-install $(SDIPC_BIN)-install clean
$(SDIPD_BIN):
	$(MAKE) -C$(SDIPD_DIR) all

$(SDIPC_BIN):
	$(MAKE) -C$(SDIPC_DIR) all

sdipd-install:
	$(MAKE) -C$(SDIPD_DIR) install

sdipc-install:
	$(MAKE) -C$(SDIPC_DIR) install

clean:
	make -C$(SDIPD_DIR) clean
	make -C$(SDIPC_DIR) clean
